import { TASK_QUANTITY_HANDLER } from "../constants/task.constant";
//mô tả những sự kiện liên quan tới task
const quantityHandle = (id)=>{
    return {
        type: TASK_QUANTITY_HANDLER,
        payload: id
    }
}
export {
    quantityHandle
}